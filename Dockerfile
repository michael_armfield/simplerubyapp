FROM ruby:2.2

RUN apt-get update && gem update --system && gem install bundler

RUN git init
RUN git remote add origin https://github.com/michaelarmfield1/SimpleRubyApp
RUN git fetch
RUN git checkout -t origin/master
RUN bundle install

EXPOSE 4567

CMD ["rackup", "-p", "4567"]